// Week 5 lab.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <conio.h>
#include <vector>
#include <algorithm>
#include <stdlib.h>

using namespace std;
int getDepth();
void calculateDepth(int[2][100], int);
int main()
{
	char ch;
	int SnowFallCounter[2] = {0,0};
	int numberOfCitys = 2;
	float totalSnowFall = 0.0;
	float AverageTemp[2] = {0.0,0.0};
	string citys[2] = {"",""};
	cout << "Please enter the name of the first city:";
	cin >> citys[0];
	cout << "Please enter the number of snowfall recordings for " + citys[0];
	cin >> SnowFallCounter[0];
	cout << "Please enter the name of the second city:";
	cin >> citys[1];
	cout << "Please enter the number of snowfall recordings for " + citys[1];
	cin >> SnowFallCounter[1];
	int snowRecords[2][100];
	for (int a=0;a < numberOfCitys;a++)
	{
		for (int b =0; b<SnowFallCounter[a];b++)
		{
			snowRecords[a][b] += getDepth();
		}
	}
	calculateDepth(snowRecords,numberOfCitys);
	ch = _getch();
	return 0;
}
int getDepth()
{
	int snowDepth;
	cin.ignore(100,'\n');
	cout << endl << "Please enter a depth: ";
	cin >> snowDepth;
	return snowDepth;
}
void calculateDepth(int Records[2][100],int numberOfCitys)
{
	int avgDepth = 0;
	for (int a = 0; a < numberOfCitys; a++)
	{
		for(int b = 0; b < sizeof Records[a];b++)
		{
			avgDepth += Records[a][b];
		}
		avgDepth /= sizeof Records[a];
		cout << "The average temperature for the cities is: " << avgDepth;
	}
}