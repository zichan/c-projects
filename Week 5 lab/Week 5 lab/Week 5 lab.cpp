
#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <conio.h>
using namespace std;

int makeMenu();
int returnSnowFall();
int calcAverage(int, int);

int main()
{
	int choice = 0;
	char ch;
	int cityOneSnow = 0;
	int cityOneCount = 0;

	int cityTwoSnow = 0;
	int cityTwoCount = 0;

	int cityOneAverage = 0;
	int cityTwoAverage = 0;
	int buffer = 0;
	while (choice != 5)
	{
		choice = makeMenu();
		switch (choice)
		{
			case 1:
				cout << endl << "Please enter the daily snowfall for city one: ";
				buffer = returnSnowFall();
				if (buffer <0)
				{
					cout << endl << "Error, cannot have a negative snowfall.";
				}
				else
				{
					cityOneSnow += buffer;
					cityOneCount++;
				}
				break;
			case 2:
				cout << endl << "Please enter the daily snowfall for city two: ";
				buffer = returnSnowFall();
				if (buffer <0)
				{
					cout << endl << "Error, cannot have a negative snowfall.";
				}
				else
				{
					cityTwoSnow += buffer;
					cityTwoCount++;
				}
				break;
			case 3:
				if (cityOneSnow > 0 )
				{
					cityOneAverage = calcAverage(cityOneSnow, cityOneCount);
					cout << endl << "The averave snowfall for city one is: " << cityOneAverage;
				}
				else
				{
					cout << endl << "City one had no snowfall.";
				}
				break;
			case 4:
				if (cityTwoSnow > 0 )
				{
					cityTwoAverage = calcAverage(cityTwoSnow, cityTwoCount);
					cout << endl << "TThe averave snowfall for city two is: " << cityTwoAverage;
				}
				else
				{
					cout << endl << "City two had no snowfall.";
				}
				break;
			default:
				cout << endl << "Please enter a valid menu number.";
				break;
		}
	}
	return 0;
}
int makeMenu()
{
	int choice = 0;
	cout << endl;
	cout << endl << "Press 1 and enter to enter the snowfall amount for city one ";
	cout << endl << "Press 2 and enter to enter the snowfall amount for city two";
	cout << endl << "Press 3 and enter for city one's snowfall average ";
	cout << endl << "Press 4 and enter for city two's snowfall average ";
	cout << endl << "Press 5 to exit";
	cout << endl << "Your menu selection: ";
	cin >> choice;
	return choice;
}
int returnSnowFall()
{
	int buffer = 0;
	cin >> buffer;
	return buffer;
}
int calcAverage(int total, int number)
{
	int average = total / number;
	return average;
}