// Week 3 lab.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

int main()
{
	int numberOfCitys = 0;
    int total = 0;
	int average = 0;
	cout << "Please enter the number of citys that you wish to calculate the average for: ";
	cin >> numberOfCitys;
	for (int a  = 0; a < numberOfCitys; a++)
	{
		int buffer = 0;
		cout << "Enter the temperature for city number " << a+1 << " :";
		cin >> buffer;
		total += buffer;
	}
	average = total / numberOfCitys;
	cout << "The average if the " << numberOfCitys << "citys is :" << total / numberOfCitys;
	return 0;
}