#include <iostream>
#include <iomanip>
#include <string>
#include <conio.h>


using std::cout;
using std::cin;
using std::string;
using std::endl;
using std::fixed;
using std::setprecision;


short getTemp ();
void calculateAverageTemperature (float, wchar_t);


int main()
{
	wchar_t loopCounter = 0;
	short numberOfWashingtonCities = 0;
	float totalTemperature = 0.0;
	float averageTemperature = 0.0;
	char ch;


	cout << endl << fixed << setprecision(2);

	cout << endl << "How many cities do you want to calculate the average temperature for: ";
	cin >> numberOfWashingtonCities;


	while (loopCounter < numberOfWashingtonCities)
	{
		totalTemperature += getTemp();
		loopCounter++;
	}


	calculateAverageTemperature(totalTemperature, loopCounter);
	

	cout << endl << endl << "Press any key to exit";
	ch = _getch();


	return 0;
}


short getTemp ()
{
	short cityInWashingtonTemperature;

	cin.ignore(100,'\n');

	cout << endl << "Please enter a temperature: ";
	cin >> cityInWashingtonTemperature;

	return cityInWashingtonTemperature;
}

void calculateAverageTemperature (float totalTemperature, wchar_t totalTemperatureValues)
{
	float avgTemp;

	avgTemp = totalTemperature / totalTemperatureValues;

	cout << "The average temperature for the cities in Washington is: " << avgTemp;
}