// grrr.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <conio.h>

using namespace std;

short getTemp();
void calculateTemperature(float, wchar_t);

int main()
{
	wchar_t loopCounter = 0;
	short numberofCities = 0;
	float totalTemp = 0.0;
	float averageTemp = 0.0;
	char ch;
	cout << endl << fixed << setprecision(2);
	cout << endl << "How many cities will be calculated?: ";
	cin >> numberofCities;
	while (loopCounter < numberofCities)
	{
		totalTemp += getTemp();
		loopCounter++;
	}
	calculateTemperature(totalTemp, loopCounter);
	cout << endl << endl << "Press any key to exit";
	ch = _getch();
	return 0;
}
short getTemp ()
{
	short cityTemp;
	cin.ignore(100,'\n');
	cout << endl << "Please enter a temperature: ";
	cin >> cityTemp;
	return cityTemp;
}
void calculateTemperature (float totalTemp, wchar_t totaltempValues)
{
	float avgTemp;
	avgTemp = totalTemp / totaltempValues;
	cout << "The average temperature for the cities is: " << avgTemp;
}