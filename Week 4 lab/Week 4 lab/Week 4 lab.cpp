// Week 4 lab.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <conio.h>
using namespace std;

int getInput(int);
void calcAverage(int, int);
int main()
{
	int numberOfCitys = 0;
	int total = 0;
	cout << "Please enter the total number of citys that you \nwould like to calculate the average for: ";
	cin >> numberOfCitys;
	for (int a = 0; a < numberOfCitys; a++)
	{ 
		total += getInput(a + 1);
	}
	calcAverage(total,numberOfCitys);
	return 0;
}
int getInput(int cityNumber)
{
	int buffer = 0;
	cout << "Enter the temperature for city " << cityNumber << ": ";
	cin >> buffer;
	return buffer;
}
void calcAverage(int total, int number)
{
	char ch;
	cout << "The average of the citys is " << (total / number);
	cout << endl << endl << "Press any key to exit";
	ch = _getch();
}