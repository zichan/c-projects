// week 2 lab.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

int main()
{
	int cityOne, cityTwo = 0;
	cout << "Please enter the temperature of the first city: ";
	cin >> cityOne;
	cout << "Please enter the temperature for the second city: ";
	cin >> cityTwo;
	if (cityOne > 120)
		cout << "City one has a temperature above 120 degrees";
	if (cityTwo > 120)
		cout << "City two has a temperature above 120 degrees";
	cout << "The average of the two citys are " + (cityOne / cityTwo)/2;
	if (cityOne > cityTwo)
		cout << "City one has the greater temperature.";
	else if (cityOne < cityTwo)
		cout << "City two has the greater temperature.";
	else
		cout << "The two citys have the same temperature.";
	return 0;
}

